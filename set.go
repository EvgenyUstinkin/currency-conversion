package main

import (
	sharedhttp "currency-conversion/entities/_shared/transports/http"

	user "currency-conversion/entities/user"
	userend "currency-conversion/entities/user/endpoints"
	userhttp "currency-conversion/entities/user/transports/http"

	currency "currency-conversion/entities/currency"
	currencyend "currency-conversion/entities/currency/endpoints"
	currencyhttp "currency-conversion/entities/currency/transports/http"
)

type InitializationSet struct {
	//1. put set here
	User     userend.InitializationSet
	Currency currencyend.InitializationSet
}

// NewInitializationSet makes endpoints set
func NewInitializationSet(
	//2. put usecase here
	userUsecase user.Usecase,
	currencyUsecase currency.Usecase,
) InitializationSet {
	return InitializationSet{
		//3. create relation between set and usecase
		User:     userend.NewInitializationSet(userUsecase),
		Currency: currencyend.NewInitializationSet(currencyUsecase),
	}
}

func getRouteConfig(endpoints InitializationSet) []sharedhttp.Route {
	//4. put route here
	res := []sharedhttp.Route{}
	res = append(res, userhttp.RouteConfig(endpoints.User)...)
	res = append(res, currencyhttp.RouteConfig(endpoints.Currency)...)

	return res
}
