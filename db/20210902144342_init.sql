-- +goose Up
-- SQL in this section is executed when the migration is applied.
-- +goose StatementBegin

create schema if not exists account;

CREATE TABLE IF NOT EXISTS account.users(
    id serial NOT NULL,
    uuid uuid DEFAULT uuid_generate_v4 () UNIQUE,
    created_at            timestamptz NOT NULL DEFAULT now(),
    updated_at            timestamptz NOT NULL DEFAULT now(),
    first_name varchar not null,
    middle_name varchar not null default '',
    last_name varchar not null,
    login varchar not null UNIQUE,
    PRIMARY KEY (id)
);

DROP TRIGGER IF EXISTS set_timestamp on account.users;
create trigger set_timestamp before update
    on
    account.users for each row execute procedure trigger_set_timestamp();

CREATE TABLE IF NOT EXISTS account.passwords(
    id serial NOT NULL,
    created_at            timestamptz NOT NULL DEFAULT now(),
    user_id int not null REFERENCES account.users(id),
    pass varchar not null,
    
    PRIMARY KEY (id)
);

create unique index if not exists login_users_uidx on account.users (login)
WHERE login != '';


CREATE TABLE IF NOT EXISTS account.tokens(
    id serial NOT NULL,
    uuid uuid DEFAULT uuid_generate_v4 () UNIQUE,
    created_at            timestamptz NOT NULL DEFAULT now(),
    expired_at            timestamptz NOT NULL,
    user_id int not null REFERENCES account.users(id),
    PRIMARY KEY (id)
);

DROP TRIGGER IF EXISTS set_timestamp on account.users;
create trigger set_timestamp before update
    on
    account.users for each row execute procedure trigger_set_timestamp();

-- +goose StatementEnd
-- +goose Down
-- SQL in this section is executed when the migration is rolled back.