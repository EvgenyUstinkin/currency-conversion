package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	errcodes "currency-conversion/entities/_shared/errors"
	sharedhttp "currency-conversion/entities/_shared/transports/http"
	"currency-conversion/entities/_shared/transports/http/errors"
	"currency-conversion/middlewares"
	"currency-conversion/swgen"

	kitep "github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/sirupsen/logrus"

	"github.com/gorilla/mux"

	httpSwagger "github.com/swaggo/http-swagger"
)

func makeRestHandler(endpointsList []sharedhttp.Route) http.Handler {
	//globalTracer := opentracing.GlobalTracer()
	router := mux.NewRouter()
	uniqueNameMap := make(map[string]bool, len(endpointsList))

	pathErrors := make([]string, 0)
	for _, r := range endpointsList {
		var handler *kithttp.Server
		var encoderResp = sharedhttp.EncodeRESTResponse
		if r.ResponseEncoder != nil {
			encoderResp = r.ResponseEncoder
		}
		nameEndpoint := strings.ReplaceAll(r.Title, " ", "")
		if uniqueNameMap[nameEndpoint] == false {
			uniqueNameMap[nameEndpoint] = true
		} else {
			pathErrors = append(pathErrors, fmt.Sprintf("Duplicated name '%s' for '%s'", r.Title, r.Path))
		}

		if r.Tag == "" {
			pathErrors = append(pathErrors, fmt.Sprintf("Please setup tag for '%s'", r.Path))
		}
		if len(r.Roles) == 0 || r.Roles[0] == "" {
			pathErrors = append(pathErrors, fmt.Sprintf("Please setup available roles for '%s', you can use 'Roles:[]string{\"any\"},' if needed", r.Path))
		}
		handler = kithttp.NewServer(
			kitep.Chain(
				middlewares.RecovererMiddleware(),
				//kittrace.TraceServer(globalTracer, nameEndpoint),
				middlewares.CheckAuthMiddleware(r.Roles),
				middlewares.LoggingMiddleware(),
			)(r.Endpoint),
			r.RequestDecoder,
			encoderResp,
			kithttp.ServerErrorEncoder(NewRestErrorEncoder(500, "unhandled internal error")),
			kithttp.ServerBefore(RequestInfoToContext),
		//kithttp.ServerBefore(jwt.InjectTempHeaderDataToContext),
		//kithttp.ServerBefore(injectCronAuthHeaderDataToContext),
		)

		router.Handle(r.Path, handler).Methods(r.HTTPMethods)
	}

	if os.Getenv("SWAGGER_ENABLED") == "true" {
		gen := NewSwgen(swgen.ServiceTypeRest, len(endpointsList))

		//router := mux.NewRouter()
		//initDefinitions(gen)

		for _, r := range endpointsList {
			if r.Tag == "HIDDEN" {
				continue
			}
			pathInf := swgen.PathItemInfo{
				Path:        r.Path,
				Method:      r.HTTPMethods,
				Title:       r.Title,
				Tag:         r.Tag,
				Description: fmt.Sprintf("Available for permissions: %s", strings.Join(r.Roles, ", ")),
				// Tag:         r.Tag,
				// Responses: map[string]swgen.ResponseObj{
				//  "default": {
				//  Description: "Unexpected error",
				//  Schema: &swgen.SchemaObj{
				//  Ref: "#/definitions/RestErrorResponse",
				//  },
				//  },
				// },
			}

			// for _, code := range r.HTTPErrorCodes {
			//  pathInf.Responses[strconv.Itoa(code)] = swgen.ResponseObj{
			//  Description: http.StatusText(code),
			//  Schema: &swgen.SchemaObj{
			//  Ref: "#/definitions/RestErrorResponse",
			//  },
			//  }
			// }

			gen.ParseDefinition(r)

			gen.SetPathItem(
				pathInf,
				r.RequestParamsStructure,
				r.RequestBodyStructure,
				r.ResponseStructure,
			)
		}
		if len(pathErrors) > 0 {
			for _, pathError := range pathErrors {
				logrus.Info(pathError)
			}
			panic(fmt.Sprintf("Exists %d path errors, please fix it, details above", len(pathErrors)))
		}
		secret := "QXCtFU24n5"

		router.HandleFunc(fmt.Sprintf("/v1/%s/swagger.json", secret), gen.ServeHTTP)

		router.PathPrefix(fmt.Sprintf("/docs/%s/", secret)).Handler(httpSwagger.Handler(
			httpSwagger.URL(fmt.Sprintf("/v1/%s/swagger.json", secret)),
			httpSwagger.DeepLinking(true),
			httpSwagger.DocExpansion("none"),
			httpSwagger.DomID("#swagger-ui"),
		))
	}

	router.HandleFunc("/version", versionHandler())

	return router
}
func versionHandler() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		res := AppVersion
		if res == "" {
			res = "-"
		}

		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(res))
	}
}

// NewRestErrorEncoder encodes any error to response-error.
func NewRestErrorEncoder(internalErrCode int, internalMessage string) func(ctx context.Context, err error, w http.ResponseWriter) {
	return func(ctx context.Context, err error, w http.ResponseWriter) {
		if r, ok := err.(errors.Http); ok {
			contentType := "application/json; charset=utf-8"
			body, marshalErr := json.Marshal(r)
			if marshalErr != nil {
				contentType = "text/plain; charset=utf-8"
				body = []byte(r.Error())
			}

			w.Header().Set("Content-Type", contentType)
			statusCode := errors.InternalCodes[r.InternalCode]
			if statusCode == 0 {
				statusCode = http.StatusBadRequest
			}
			w.WriteHeader(statusCode)
			w.Write(body)
			return
		}

		errorMessage := err.Error()
		handlerMessageUnavailable := len(err.Error()) == 0
		if handlerMessageUnavailable {
			errorMessage = internalMessage
		}
		if strings.Contains(errorMessage, "yandexcloud") || strings.Contains(errorMessage, "no such host") {
			errorMessage = "ошибка подключения к бд"
		}

		contentType := "application/json; charset=utf-8"
		httpError := errors.NewHttpError(errcodes.CodeInternalError, errorMessage)

		body, _ := json.Marshal(httpError)

		w.Header().Set("Content-Type", contentType)
		w.WriteHeader(internalErrCode)
		w.Write(body)
	}
}

// NewSwgen creates new swgen generator
func NewSwgen(serviceType swgen.ServiceType, endpointCount int) *swgen.Generator {
	const (
		// XServiceType is a swagger vendor extension
		XServiceType = `x-service-type`
		// XAttachVersionToHead is a swagger vendor extension
		XAttachVersionToHead = `x-attach-version-to-head`
	)

	gen := swgen.NewGenerator()
	gen.SetHost("")
	gen.SetBasePath("/")
	gen.SetInfo(fmt.Sprintf("API version 1.0, endpoints count is %d", endpointCount), "", "", "1.0")
	gen.SetContact("", "", "")
	gen.IndentJSON(true)

	gen.AddExtendedField(XServiceType, serviceType)
	gen.AddExtendedField(XAttachVersionToHead, false)

	return gen
}

func RequestInfoToContext(ctx context.Context, r *http.Request) context.Context {
	ctxxxx := ctx
	ctxxxx = context.WithValue(ctxxxx, "ApiToken", r.Header.Get("Authorization"))
	ctxxxx = context.WithValue(ctxxxx, "RequestURL", r.RequestURI)
	ctxxxx = context.WithValue(ctxxxx, "ClientIP", GetIP(r))
	ctxxxx = context.WithValue(ctxxxx, "CaptchaToken", r.Header.Get("X-Recaptcha-Token"))
	ctxxxx = context.WithValue(ctxxxx, "Referrer", r.Referer())
	ctxxxx = context.WithValue(ctxxxx, "RequestMethod", r.Method)
	return ctxxxx
}
func GetIP(r *http.Request) string {
	IPAddress := r.Header.Get("X-Real-Ip")
	if IPAddress == "" {
		IPAddress = r.Header.Get("X-Forwarded-For")
	}
	if IPAddress == "" {
		IPAddress = r.RemoteAddr
	}
	return IPAddress
}
