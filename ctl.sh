IMAGE_NAME=registry.gitlab.com/evgenyustinkin/currency-conversion:latest
CONTAINER_LOCAL_RUN_NAME=CurrencyConversionAPI 

export CURRENT_HOST_NAME=localhost
export REDIS_HOST=""

export DATABASE_HOST=""
export DATABASE_HOST_SLAVE1="" 
export DATABASE_NAME=""
export DATABASE_PORT=""
export DATABASE_USER=""
export DATABASE_PASSWORD=""
export DATABASE_SSL_MODE=""
export DATABASE_SSL_CERT=""

export SWAGGER_ENABLED="true"

usage() {
  cat <<EOF
Usage:

  sh ./$(basename $0) --migrate  run sql migrations
  sh ./$(basename $0) --run      build and run application
  sh ./$(basename $0) --prod     deploy to prod
EOF
  exit 1
}

log() {
  local DATETIME=`date '+%Y/%m/%d %H:%M:%S'`
  echo "${DATETIME} ${1}"
}

export_ldflags() {
	build_date=$(date +"%d.%m_%H:%M")
	git_rev=$(git rev-parse --short HEAD)
  
	export LDFLAGS="-X main.AppVersion=${build_date}_${git_rev}"

  echo "${LDFLAGS}"
}

migrate() {
  log "Run migrations"
  goose -dir="./db" postgres "host=${DATABASE_HOST} port=${DATABASE_PORT} user=${DATABASE_USER} password=${DATABASE_PASSWORD} database=${DATABASE_NAME} sslmode=${DATABASE_SSL_MODE} sslrootcert=${DATABASE_SSL_CERT}" up -v
}

build() {
  log "Run Make build: [${IMAGE_NAME}]"
	export_ldflags
  docker build -t ${IMAGE_NAME} . --build-arg LDFLAGS="${LDFLAGS}"
}

push() {
  log "Push docker image: [${IMAGE_NAME}]"
  docker login --username oauth --password $YANDEX_TOKEN registry.gitlab.com
  docker push ${IMAGE_NAME}
}

run_docker() {
  if [[ "$(docker ps -q -f name=${CONTAINER_LOCAL_RUN_NAME})" ]]; then
    log "Docker container with name: '${CONTAINER_LOCAL_RUN_NAME}' already exists. Now stop it.."
    docker stop ${CONTAINER_LOCAL_RUN_NAME}
    log "done!"
  fi

  log "Run docker container with name: '${CONTAINER_LOCAL_RUN_NAME}'"
  docker run -it --rm --name ${CONTAINER_LOCAL_RUN_NAME} \
    ${IMAGE_NAME}

  exit_code=$?
  if [[ ${exit_code} != 0 ]]; then
    exit ${exit_code}
  fi
}

run_local() {
  clear
  CURRENT_HOST_NAME="LOCALHOST"
  log "Build and run application at $CURRENT_HOST_NAME. Please wait.."
  go run *.go
}

case "$1" in
  "--build")
    build
    ;;
  "--docker")
    build
    run_docker
    ;;
  "--migrate")
    migrate
    ;;
  "--push")
    build
    push
    ;;
  "--ldf")
    export_ldflags
    ;;
  "--run")
    run_local
    ;;
  *)
    usage
    ;;
esac
