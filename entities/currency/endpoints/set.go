package endpoints

import (
	"currency-conversion/entities/currency"

	"github.com/go-kit/kit/endpoint"
)

type InitializationSet struct {
	GetAllPairs endpoint.Endpoint
}

// NewInitializationSet makes endpoints set
func NewInitializationSet(svc currency.Usecase) InitializationSet {
	return InitializationSet{
		GetAllPairs: GetAllPairs(svc),
	}
}
