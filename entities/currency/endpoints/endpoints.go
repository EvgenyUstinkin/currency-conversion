package endpoints

import (
	"context"

	"currency-conversion/entities/currency"

	"github.com/go-kit/kit/endpoint"
)

func GetAllPairs(svc currency.Usecase) endpoint.Endpoint {
	return func(ctx context.Context, requ interface{}) (interface{}, error) {
		return svc.GetAllPairs(ctx)
	}
}
