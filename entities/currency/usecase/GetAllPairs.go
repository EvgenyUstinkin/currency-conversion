package service

import (
	"context"
	"currency-conversion/entities/currency/models/response"
)

func (svc *usecase) GetAllPairs(ctx context.Context) ([]response.CurrencyItem, error) {
	res, err := svc.repo.GetAllPairs(ctx)
	if err != nil {
		return nil, err
	}
	return res, nil
}
