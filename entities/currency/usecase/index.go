package service

import (
	"currency-conversion/entities/currency"
)

type usecase struct {
	repo currency.Repository
}

func New(repo currency.Repository) currency.Usecase {
	return &usecase{
		repo: repo,
	}
}
