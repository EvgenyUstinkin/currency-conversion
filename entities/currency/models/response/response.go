package response

type CurrencyItem struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}
