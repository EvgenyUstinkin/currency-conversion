package currency

import (
	"context"
	baserepo "currency-conversion/entities/_shared/repositories"
	"currency-conversion/entities/currency/models/response"
)

type Repository interface {
	baserepo.BaseRepository

	GetAllPairs(ctx context.Context) ([]response.CurrencyItem, error)
}
