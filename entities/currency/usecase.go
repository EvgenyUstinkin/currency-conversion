package currency

import (
	"context"
	"currency-conversion/entities/currency/models/response"
)

type Usecase interface {
	UpgradeLatestPriceValue(ctx context.Context, uuid string) error
	GetAllPairs(ctx context.Context) ([]response.CurrencyItem, error)
}
