package repositories

import (
	"context"
	"currency-conversion/entities/currency/models/response"
	"database/sql"

	"currency-conversion/entities/_shared/utils"

	"github.com/Masterminds/squirrel"
	"github.com/sirupsen/logrus"
)

func (s *repository) GetAllPairs(ctx context.Context) ([]response.CurrencyItem, error) {
	res := make([]response.CurrencyItem, 0)

	query := squirrel.Select(
		"uuid",
		"name",
	).From("TABLE")

	query = query.Where("active = ?", true).
		PlaceholderFormat(squirrel.Dollar).RunWith(s.SlaveNode(ctx))

	rows, err := query.QueryContext(ctx)
	if rows != nil {
		defer rows.Close()
	}
	if err != nil {
		if err == sql.ErrNoRows {
			return res, nil
		}
		q, p, _ := query.ToSql()
		logrus.Error(utils.SqlErrLogMsg(err, q, p))
		return res, err
	}

	for rows.Next() {
		item := response.CurrencyItem{}
		err := rows.Scan(
			&item.UUID,
			&item.Name,
		)
		if err != nil {
			return nil, err
		}
		res = append(res, item)
	}
	return res, nil
}
