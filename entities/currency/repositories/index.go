package repositories

import (
	"context"
	"math/rand"

	baserepo "currency-conversion/entities/_shared/repositories"

	"github.com/Masterminds/squirrel"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
)

type repository struct {
	red *redis.Client
	db  []*sqlx.DB
}

func (r *repository) MasterNode() *sqlx.DB {
	return r.db[0]
}
func (r *repository) SlaveNode(ctx context.Context) squirrel.BaseRunner {
	if baserepo.CheckTranExists(ctx) {
		return r.GetCurrentTran(ctx)
	}
	index := 0
	if len(r.db) > 1 {
		index = rand.Intn(len(r.db)-1) + 1
	}
	return r.db[index].DB
}

func (r *repository) BeginTran(ctx context.Context) context.Context {
	return baserepo.BeginTran(ctx, r.MasterNode())
}
func (r *repository) CommitTran(ctx context.Context) error {
	return baserepo.CommitTran(ctx)
}
func (r *repository) RollbackTran(ctx context.Context) error {
	return baserepo.RollbackTran(ctx)
}
func (r *repository) GetCurrentTran(ctx context.Context) *sqlx.Tx {
	return baserepo.GetCurrentTran(ctx)
}
func (r *repository) CheckTranExists(ctx context.Context) bool {
	return baserepo.CheckTranExists(ctx)
}
func (r *repository) ClearCache(ctx context.Context, key, prefix string) {
	baserepo.ClearCache(ctx, key, prefix, r.red)
}
func (r *repository) SaveToCacheForever(ctx context.Context, key, prefix string, obj interface{}) {
	baserepo.SaveToCacheForever(ctx, key, prefix, obj, r.red)
}
func (r *repository) SaveToCacheSec(ctx context.Context, key, prefix string, seconds int, obj interface{}) {
	baserepo.SaveToCacheSec(ctx, key, prefix, seconds, obj, r.red)
}
func (r *repository) GetFromCache(ctx context.Context, key, prefix string, obj interface{}) {
	baserepo.GetFromCache(ctx, key, prefix, obj, r.red)
}

func New(db []*sqlx.DB,
	red *redis.Client,
) *repository {
	return &repository{
		red: red,
		db:  db,
	}
}
