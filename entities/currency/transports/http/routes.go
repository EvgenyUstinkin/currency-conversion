package http

import (
	sharedhttp "currency-conversion/entities/_shared/transports/http"
	"currency-conversion/entities/currency/endpoints"
	"currency-conversion/entities/currency/models/response"
)

func RouteConfig(endpoints endpoints.InitializationSet) []sharedhttp.Route {
	return []sharedhttp.Route{

		{
			Endpoint:             endpoints.GetAllPairs,
			HTTPMethods:          "GET",
			Tag:                  "currency",
			Roles:                []string{"admin"},
			Path:                 "/v1/currency/list",
			RequestBodyStructure: nil,
			RequestDecoder:       sharedhttp.DecodeDefault,
			ResponseStructure:    []response.CurrencyItem{},
			Title:                "Get all pairs",
		},
	}
}
