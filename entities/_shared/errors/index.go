package errors

const (
	CodeInvalidRequest = iota + 10000
	CodeUnauthorized
	CodeNotFound
	CodeForbidden
	CodeConflict
)

const (
	CodeInternalError = iota + 1
)
