package http

import (
	"context"
	"io/ioutil"
	"net/http"
	"strconv"

	errcodes "currency-conversion/entities/_shared/errors"
	"currency-conversion/entities/_shared/transports/http/errors"

	"github.com/gorilla/mux"
)

func DecodePlainText(_ context.Context, r *http.Request) (a interface{}, e error) {
	bodyBytes, _ := ioutil.ReadAll(r.Body)
	bodyString := string(bodyBytes)
	return bodyString, nil
}
func DecodeDefault(_ context.Context, r *http.Request) (a interface{}, e error) {
	return
}

func DecodeUuid(_ context.Context, r *http.Request) (a interface{}, e error) {
	params := mux.Vars(r)
	if params["uuid"] == "" {
		return nil, errors.NewHttpError(errcodes.CodeInvalidRequest, "invalid uuid")
	}

	return params["uuid"], nil
}

func DecodeIdInt64(_ context.Context, r *http.Request) (interface{}, error) {
	params := mux.Vars(r)
	if params["id"] == "" {
		return nil, errors.NewHttpError(errcodes.CodeInvalidRequest, "id is required")
	}
	n, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		return nil, errors.NewHttpError(errcodes.CodeInvalidRequest, "invalid id")
	}
	return n, nil
}

func DecodeIdInt(_ context.Context, r *http.Request) (interface{}, error) {
	params := mux.Vars(r)
	if params["id"] == "" {
		return nil, errors.NewHttpError(errcodes.CodeInvalidRequest, "id is required")
	}
	n, err := strconv.Atoi(params["id"])
	if err != nil {
		return nil, errors.NewHttpError(errcodes.CodeInvalidRequest, "invalid id")
	}
	return n, nil
}
