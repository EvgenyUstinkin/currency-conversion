package http

import (
	"context"
	"encoding/json"
	"net/http"
)

func EncodeRESTResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	// if reflect.ValueOf(response).IsNil() {
	// 	w.Write([]byte("{}"))
	// 	return nil
	// }
	return json.NewEncoder(w).Encode(response)
}

func EncodeHtmlResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Write([]byte(response.(string)))
	return nil
}
