package errors

import (
	"net/http"

	errcodes "currency-conversion/entities/_shared/errors"
)

var InternalCodes = map[int]int{
	errcodes.CodeUnauthorized: http.StatusUnauthorized,
	errcodes.CodeNotFound:     http.StatusNotFound,
	errcodes.CodeForbidden:    http.StatusForbidden,
	errcodes.CodeConflict:     http.StatusConflict,
}

type Http struct {
	StatusCode   int    `json:"-"`
	InternalCode int    `json:"-"`
	Msg          string `json:"message,omitempty"`
}

func NewHttpError(code int, msg string) Http {
	return Http{
		StatusCode:   http.StatusBadRequest,
		InternalCode: code,
		Msg:          msg,
	}
}

func (h Http) Error() string {
	return h.Msg
}
