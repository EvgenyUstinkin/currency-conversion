package http

import (
	kitep "github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"
)

type Route struct {
	Title                  string
	Roles                  []string
	Path                   string
	HTTPMethods            string
	Tag                    string
	Endpoint               kitep.Endpoint
	RequestDecoder         kithttp.DecodeRequestFunc
	ResponseEncoder        kithttp.EncodeResponseFunc
	RequestParamsStructure interface{}
	RequestBodyStructure   interface{}
	ResponseStructure      interface{}

	HTTPErrorCodes []int
}
