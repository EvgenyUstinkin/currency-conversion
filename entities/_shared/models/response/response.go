package response

type DefaultResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message,omitempty"`
}
