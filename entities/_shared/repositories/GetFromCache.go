package repositories

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
	"github.com/pquerna/ffjson/ffjson"
	"github.com/sirupsen/logrus"
)

func GetFromCache(ctx context.Context, key, prefix string, obj interface{}, r *redis.Client) {
	pref := fmt.Sprintf("%s_%s", key, prefix)
	s, err := r.Get(ctx, pref).Result()
	if err == nil {
		err = ffjson.Unmarshal([]byte(s), &obj)
		if err != nil {
			logrus.Error("GetFromCache ", err)
		}
	} else {
		obj = nil
	}
}
