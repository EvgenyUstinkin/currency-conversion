package repositories

import (
	"context"
	"errors"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

type contextKey int

const (
	txKey contextKey = iota
	txDepthKey
)

func BeginTran(ctx context.Context, db *sqlx.DB) context.Context {
	count := 0
	txCtx := ctx

	if _, ok := ctx.Value(txKey).(*sqlx.Tx); ok {
		count, _ = ctx.Value(txDepthKey).(int)
		count++
	} else {
		tx, err := db.BeginTxx(ctx, nil)
		if err != nil {
			time.Sleep(500 * time.Millisecond)
			//logrus.Error("BeginTran panic ", err)
			return BeginTran(ctx, db)
		}
		txCtx = context.WithValue(ctx, txKey, tx)
	}
	txCtx = context.WithValue(txCtx, txDepthKey, count)

	return txCtx

}

func CheckTranExists(ctx context.Context) bool {
	if _, ok := ctx.Value(txKey).(*sqlx.Tx); ok {
		return true
	}
	return false
}

func CommitTran(ctx context.Context) error {
	if tx, ok := ctx.Value(txKey).(*sqlx.Tx); ok {
		if count, ok := ctx.Value(txDepthKey).(int); ok && count == 0 {
			return tx.Commit()
		}
	}
	return nil
}

func RollbackTran(ctx context.Context) error {
	if tx, ok := ctx.Value(txKey).(*sqlx.Tx); ok {
		return tx.Rollback()
	}

	return errors.New("at current context transaction was not defined")
}

func GetCurrentTran(ctx context.Context) *sqlx.Tx {
	if tx, ok := ctx.Value(txKey).(*sqlx.Tx); ok {
		return tx
	}

	logrus.Error("GetCurrentTran panic ", "at current context transaction was not defined")
	return nil
}
