package repositories

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/pquerna/ffjson/ffjson"
	"github.com/sirupsen/logrus"
)

func SaveToCache(ctx context.Context, key, prefix string, obj interface{}, r *redis.Client, dur time.Duration) {
	pref := fmt.Sprintf("%s_%s", key, prefix)
	xType := fmt.Sprintf("%T", obj)
	var json []byte
	if xType == "string" {
		json = []byte(obj.(string))
	} else {
		json, _ = ffjson.Marshal(obj)
	}

	if dur == 0 {
		dur = 60 * 60 * time.Second
	}
	err := r.SetEX(context.Background(), pref, json, dur).Err()
	if err != nil {
		logrus.Error("SaveToCache ", err)
	}
}

func ClearCache(ctx context.Context, key, prefix string, r *redis.Client) {
	SaveToCache(ctx, key, prefix, "", r, 1*time.Second)
}
func SaveToCacheSec(ctx context.Context, key, prefix string, seconds int, obj interface{}, r *redis.Client) {
	SaveToCache(ctx, key, prefix, obj, r, time.Duration(seconds)*time.Second)
}
func SaveToCacheForever(ctx context.Context, key, prefix string, obj interface{}, r *redis.Client) {
	SaveToCache(ctx, key, prefix, obj, r, 0)
}
