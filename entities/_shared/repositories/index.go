package repositories

import (
	"context"

	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

type BaseRepository interface {
	MasterNode() *sqlx.DB
	SlaveNode(ctx context.Context) squirrel.BaseRunner

	BeginTran(ctx context.Context) context.Context
	CommitTran(context.Context) error
	RollbackTran(context.Context) error
	CheckTranExists(ctx context.Context) bool
	GetCurrentTran(context.Context) *sqlx.Tx

	SaveToCacheSec(ctx context.Context, key, prefix string, seconds int, obj interface{})
	SaveToCacheForever(ctx context.Context, key, prefix string, obj interface{})
	GetFromCache(ctx context.Context, key, prefix string, obj interface{})
	ClearCache(ctx context.Context, key, prefix string)
}
