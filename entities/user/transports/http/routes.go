package http

import (
	sharedhttp "currency-conversion/entities/_shared/transports/http"
	"currency-conversion/entities/user/endpoints"
	"currency-conversion/entities/user/models/request"
	"currency-conversion/entities/user/models/response"
)

func RouteConfig(endpoints endpoints.InitializationSet) []sharedhttp.Route {
	return []sharedhttp.Route{

		{
			Endpoint:             endpoints.LoginWitCreds,
			HTTPMethods:          "POST",
			Tag:                  "auth",
			Roles:                []string{"any"},
			Path:                 "/v1/auth/creds",
			RequestBodyStructure: request.LoginWitCreds{},
			RequestDecoder:       decodeLoginWitCreds,
			ResponseStructure:    response.AuthSuccess{},
			Title:                "Login via credentials",
		},
	}
}
