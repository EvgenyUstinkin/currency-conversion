package http

import (
	"context"
	"encoding/json"
	"net/http"
	"regexp"
	"unicode"

	errcodes "currency-conversion/entities/_shared/errors"
	"currency-conversion/entities/_shared/transports/http/errors"
	"currency-conversion/entities/user/models/request"
)

var (
	emailRegexp = regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,10}$`)
)

func decodeLoginWitCreds(_ context.Context, r *http.Request) (interface{}, error) {
	var request request.LoginWitCreds

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, errors.NewHttpError(errcodes.CodeInvalidRequest, err.Error())
	}
	return request, nil
}

func PasswordValidator(pwd string) error {
	if pwd == "" {
		return errors.NewHttpError(errcodes.CodeInvalidRequest, "Password is empty")
	}

	var mustHave = []func(rune) bool{
		unicode.IsUpper,
		unicode.IsLower,
		unicode.IsDigit,
	}
	for _, testRune := range mustHave {
		found := false
		for _, r := range pwd {
			if testRune(r) {
				found = true
			}
		}
		if !found {
			return errors.NewHttpError(errcodes.CodeInvalidRequest, "Password should contains 1 upper, 1 lower, 1 digit symbols and more 6 symbols")
		}
	}
	if len([]rune(pwd)) < 6 {
		return errors.NewHttpError(errcodes.CodeInvalidRequest, "Password can't be less 6 symbols")
	}
	return nil
}
