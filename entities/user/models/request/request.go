package request

type LoginWitCreds struct {
	Email      string `json:"email"`
	Password   string `json:"password"`
	Rememberme bool   `json:"rememberme"`
}

type RefreshToken struct {
	RefreshToken string `json:"refresh_token"`
}
