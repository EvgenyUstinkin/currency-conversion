package response

type AuthSuccess struct {
	Success     bool        `json:"success"`
	Credentials Credentials `json:"credentials,omitempty"`
}
type Credentials struct {
	AccessToken          string `json:"access_token,omitempty"`
	AccessTokenExpiresIn int64  `json:"access_token_expires_in,omitempty"`
}

type CurrentUserRoles struct {
	Roles []string `json:"roles"`
}
