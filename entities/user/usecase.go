package user

import (
	"context"

	"currency-conversion/entities/user/models/request"
	"currency-conversion/entities/user/models/response"
)

type Usecase interface {
	VerifyAuthToken(ctx context.Context) (bool, error)
	LoginWitCreds(ctx context.Context, req request.LoginWitCreds) (*response.AuthSuccess, error)
	GetCurrentUserRoles(ctx context.Context) (*response.CurrentUserRoles, error)
}
