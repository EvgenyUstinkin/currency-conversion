package user

import (
	"context"
	baserepo "currency-conversion/entities/_shared/repositories"
)

type Repository interface {
	baserepo.BaseRepository

	GetLastPassword(ctx context.Context, userID int) (pass string, err error)
}
