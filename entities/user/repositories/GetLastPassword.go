package repositories

import (
	"context"
	"database/sql"

	"currency-conversion/entities/_shared/utils"

	"github.com/Masterminds/squirrel"
	"github.com/sirupsen/logrus"
)

func (s *repository) GetLastPassword(ctx context.Context, userID int) (string, error) {
	res := ""
	q := squirrel.Select(
		"pass",
	).From("account.passwords").Where("user_id = ?", userID).
		OrderBy("id desc").Limit(1).
		PlaceholderFormat(squirrel.Dollar).RunWith(s.SlaveNode(ctx))
	row := q.QueryRowContext(ctx)
	err := row.Scan(
		&res,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return res, nil
		}
		q1, p, _ := q.ToSql()
		logrus.Error(utils.SqlErrLogMsg(err, q1, p))
		return res, err
	}
	return res, nil
}
