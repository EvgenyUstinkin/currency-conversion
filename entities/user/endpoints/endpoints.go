package endpoints

import (
	"context"

	"currency-conversion/entities/user"
	"currency-conversion/entities/user/models/request"

	"github.com/go-kit/kit/endpoint"
)

func LoginWitCreds(svc user.Usecase) endpoint.Endpoint {
	return func(ctx context.Context, requ interface{}) (interface{}, error) {
		req, _ := requ.(request.LoginWitCreds)
		return svc.LoginWitCreds(ctx, req)
	}
}
