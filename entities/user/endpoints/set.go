package endpoints

import (
	"currency-conversion/entities/user"

	"github.com/go-kit/kit/endpoint"
)

type InitializationSet struct {
	LoginWitCreds endpoint.Endpoint
}

// NewInitializationSet makes endpoints set
func NewInitializationSet(svc user.Usecase) InitializationSet {
	return InitializationSet{
		LoginWitCreds: LoginWitCreds(svc),
	}
}
