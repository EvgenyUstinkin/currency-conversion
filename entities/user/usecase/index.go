package service

import (
	"currency-conversion/entities/user"
)

type usecase struct {
	repo user.Repository
}

func New(repo user.Repository) user.Usecase {
	return &usecase{
		repo: repo,
	}
}
