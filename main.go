package main

import (
	"context"
	"currency-conversion/daemon"
	"currency-conversion/middlewares"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime/debug"
	"sync"
	"syscall"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/rs/cors"

	userrepo "currency-conversion/entities/user/repositories"
	userusecase "currency-conversion/entities/user/usecase"

	currencyrepo "currency-conversion/entities/currency/repositories"
	currencyusecase "currency-conversion/entities/currency/usecase"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"

	_ "github.com/lib/pq"
)

var (
	AppVersion string // this value set by compiler
)

func newRedisPool() *redis.Client {
	var ctx = context.Background()
	rdb := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_HOST"),
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	err := rdb.Set(ctx, "key", "value", 0).Err()
	if err != nil {
		//panic(err)
	}
	return rdb
}

func main() {
	dispatcher()
	debug.SetPanicOnFault(true)
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})
	logrus.SetLevel(logrus.DebugLevel)

	redPool := newRedisPool()
	defer func() {
		for _, intDb := range middlewares.InternalDbs {
			intDb.Close()
		}
	}()

	port := 8080
	dsn := fmt.Sprintf("host=%s port=%s database=%s user=%s password=%s sslmode=%s sslrootcert=%s",
		os.Getenv("DATABASE_HOST"),
		os.Getenv("DATABASE_PORT"),
		os.Getenv("DATABASE_NAME"),
		os.Getenv("DATABASE_USER"),
		os.Getenv("DATABASE_PASSWORD"),
		os.Getenv("DATABASE_SSL_MODE"),
		os.Getenv("DATABASE_SSL_CERT"))
	master, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		logrus.Fatal("master db connection", err)
	}
	defer master.Close()

	dbs := []*sqlx.DB{master}
	{
		if os.Getenv("DATABASE_HOST_SLAVE1") != "" {
			dsnSlave1 := fmt.Sprintf("host=%s port=%s database=%s user=%s password=%s sslmode=%s sslrootcert=%s",
				os.Getenv("DATABASE_HOST_SLAVE1"),
				os.Getenv("DATABASE_PORT"),
				os.Getenv("DATABASE_NAME"),
				os.Getenv("DATABASE_USER"),
				os.Getenv("DATABASE_PASSWORD"),
				os.Getenv("DATABASE_SSL_MODE"),
				os.Getenv("DATABASE_SSL_CERT"))
			slave1, err := sqlx.Connect("postgres", dsnSlave1)
			if err != nil {
				logrus.Fatal("slave1 db connection", err)
			}
			defer slave1.Close()
			dbs = append(dbs, slave1)
		}
	}

	useruc := userusecase.New(userrepo.New(dbs, redPool))
	currencyuc := currencyusecase.New(currencyrepo.New(dbs, redPool))

	initializationSet := NewInitializationSet(
		useruc,
		currencyuc,
	)

	daemonService := daemon.NewDaemonService(
		currencyuc,
	)
	daemonService.DaemonRun(context.Background())

	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		logrus.Printf("Server started at %d port", port)
		logrus.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), configureCors().Handler(makeRestHandler(getRouteConfig(initializationSet)))))
		wg.Done()
	}()
	logrus.Info(fmt.Sprintf("Application started at %s", os.Getenv("CURRENT_HOST_NAME")))
	wg.Wait()
}

func dispatcher() {
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		logrus.Info(fmt.Sprintf("Application gracefully shutdown at %s", os.Getenv("CURRENT_HOST_NAME")))
		select {
		case <-time.After(2 * time.Second):
			os.Exit(0)
			break
		}
	}()
}

func configureCors() *cors.Cors {
	return cors.New(cors.Options{
		AllowedOrigins: []string{"*"}, //you service is available and allowed for this base url
		AllowedMethods: []string{
			http.MethodGet, //http methods for your app
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},
		AllowedHeaders: []string{
			//"Authorization",
			//"Content-Type",
			"*", //or you can your header key values which you are using in your application
		},
	})
}
