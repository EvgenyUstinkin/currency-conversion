package daemon

import (
	"context"
	"currency-conversion/entities/currency"
)

type daemonService struct {
	currency currency.Usecase
}

func NewDaemonService(
	currencyuc currency.Usecase,
) DService {
	return &daemonService{
		currency: currencyuc,
	}
}

type DService interface {
	DaemonRun(ctx context.Context)
}
