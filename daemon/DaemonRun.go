package daemon

import (
	"context"
	"fmt"
	"runtime/debug"

	"github.com/robfig/cron"
	"github.com/sirupsen/logrus"
)

func recoverForThreadRunJobWithLocks() {
	if r := recover(); r != nil {
		logrus.Error("recoverForThreadRunJobWithLocks ", "error: ", r, " stack: ", string(debug.Stack()))
	}
}

// Run jobs.
func (s *daemonService) DaemonRun(ctx context.Context) {
	c := cron.New()

	currencyPairsForUpdate := make(chan string)
	go func() {
		for uuid := range currencyPairsForUpdate {
			go func(mmm string) {
				defer recoverForThreadRunJobWithLocks()
				logrus.Debug("UpgradeLatestPriceValue", " ", mmm)
				errr := s.currency.UpgradeLatestPriceValue(ctx, mmm)
				if errr != nil {
					logrus.Error("UpgradeLatestPriceValue", " ", mmm, " ", errr)
				}
			}(uuid)
		}
	}()

	c.AddFunc(fmt.Sprintf("@every %ds", 60*60 /*every hour*/), func() {
		ctx := context.Background()
		pairs, err := s.currency.GetAllPairs(ctx)
		if err != nil {
			logrus.Error("GetAllPairs", " ", err)
		}
		for _, pair := range pairs {
			currencyPairsForUpdate <- pair.UUID
		}
	})

	c.Start()
	logrus.Debug("Jobs started")
}
