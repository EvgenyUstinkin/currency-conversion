FROM golang:1.16.2

RUN mkdir /app
WORKDIR /app
ARG LDFLAGS=""

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# Build the binary
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "$LDFLAGS" -a -installsuffix cgo -o /go/bin/app

ENTRYPOINT ["/go/bin/app"]