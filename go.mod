module currency-conversion

go 1.16

require (
	github.com/Masterminds/squirrel v1.5.0
	github.com/go-kit/kit v0.10.0
	github.com/go-redis/redis/v8 v8.11.3
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.2
	github.com/pquerna/ffjson v0.0.0-20190930134022-aa0246cd15f7
	github.com/robfig/cron v1.2.0
	github.com/rs/cors v1.8.0
	github.com/sirupsen/logrus v1.4.2
	github.com/swaggo/http-swagger v1.1.1
)
