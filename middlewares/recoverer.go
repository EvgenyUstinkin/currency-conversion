package middlewares

import (
	"context"
	"fmt"
	"runtime"

	errcodes "currency-conversion/entities/_shared/errors"
	"currency-conversion/entities/_shared/transports/http/errors"

	"github.com/go-kit/kit/endpoint"
	"github.com/sirupsen/logrus"
)

// RecovererMiddleware recover endpoint panic.
func RecovererMiddleware() endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {

		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			defer func() {
				if p := recover(); p != nil {
					logrus.Error("panic recovered: ", p, " Stack trace: ", trace())
					err =
						errors.NewHttpError(errcodes.CodeInternalError, fmt.Sprintf("Unhandled exception, please contact support"))

				}
			}()

			return next(ctx, request)
		}
	}
}

func trace() string {
	buf := make([]byte, 2<<14)
	n := runtime.Stack(buf, false)
	return string(buf[:n])
}
