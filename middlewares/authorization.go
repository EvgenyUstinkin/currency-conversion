package middlewares

import (
	"context"
	"fmt"
	"os"
	"strings"

	errcodes "currency-conversion/entities/_shared/errors"
	"currency-conversion/entities/_shared/transports/http/errors"

	userrepo "currency-conversion/entities/user/repositories"
	userusecase "currency-conversion/entities/user/usecase"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

var internalRdp *redis.Client
var InternalDbs []*sqlx.DB

func newRedisPool() *redis.Client {
	if internalRdp != nil {
		return internalRdp
	}

	var ctx = context.Background()
	rdb := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_HOST"),
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	err := rdb.Set(ctx, "key", "value", 0).Err()
	if err != nil {
		logrus.Error("newRedisPool ", err)
	}
	internalRdp = rdb
	return rdb
}
func newPgPool() []*sqlx.DB {
	if len(InternalDbs) > 0 {
		return InternalDbs
	}

	dsn := fmt.Sprintf("host=%s port=%s database=%s user=%s password=%s sslmode=%s sslrootcert=%s",
		os.Getenv("DATABASE_HOST"),
		os.Getenv("DATABASE_PORT"),
		os.Getenv("DATABASE_NAME"),
		os.Getenv("DATABASE_USER"),
		os.Getenv("DATABASE_PASSWORD"),
		os.Getenv("DATABASE_SSL_MODE"),
		os.Getenv("DATABASE_SSL_CERT"))
	master, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		logrus.Fatal("master db connection", err)
	}

	InternalDbs = []*sqlx.DB{master}
	return InternalDbs
}

func CheckAuthMiddleware(roles []string) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			//------------------//------------------//------------------
			redPool := newRedisPool()
			dbs := newPgPool()
			useruc := userusecase.New(userrepo.New(dbs, redPool))
			//------------------//------------------//------------------
			if len(roles) == 0 {
				return nil, errors.NewHttpError(errcodes.CodeUnauthorized, fmt.Sprintf("roles for endpoint don't set"))
			}
			if roles[0] == "any" {
				return next(ctx, request)
			}

			valid, err := useruc.VerifyAuthToken(ctx)
			if err != nil {
				return nil, err
			}
			if !valid {
				return nil, errors.NewHttpError(errcodes.CodeUnauthorized, fmt.Sprintf("Auth token invalid or expired"))
			}
			{
				usr, _ := useruc.GetCurrentUserRoles(ctx)

				for _, permission := range usr.Roles {
					for _, role := range roles {
						if permission == role {
							return next(ctx, request)
						}
					}
				}
				logrus.Printf("access denied \nrequired one of: %s \ncurrent user have: %s \n", strings.Join(roles, ", "), strings.Join(usr.Roles, ", "))
				return nil, errors.NewHttpError(errcodes.CodeForbidden, fmt.Sprintf("Access denied"))
			}
		}
	}
}
