package middlewares

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/sirupsen/logrus"
)

// LoggingMiddleware returns an endpoint logging middleware
func LoggingMiddleware() endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			defer func(begin time.Time) {
				go func() {
					took := float64(time.Since(begin)/time.Microsecond) / 1000

					requestURL := fmt.Sprintf("[%s] %s", ctx.Value("RequestMethod"), ctx.Value("RequestURL"))

					logrus.Printf(`url: %s; user_uuid: %s; took: %sms;`,
						requestURL,
						ctx.Value("CURRENT_USER_UUID"),
						strconv.FormatFloat(took, 'f', 3, 64),
					)
				}()
			}(time.Now())

			return next(ctx, request)
		}
	}
}
